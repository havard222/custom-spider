# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: http://doc.scrapy.org/en/latest/topics/item-pipeline.html
from cscore.dynamodb_models import DynamoDB
from custom_spider import settings
from django.conf import settings as d_settings

d_settings.configure(settings)

class CrawlerPipeline(object):

    def open_spider(self, spider):
        print '#' * 30
        print 'START SPIDER: %s' % spider.name

    def close_spider(self, spider):
        print '#' * 30
        print '# stats'
        print spider.crawler.stats.get_stats()
        print
        print 'CLOSE SPIDER: %s' % spider.name
        print '#' * 30

    def process_item(self, item, spider):
        item['spider'] = spider.name
        item['retailer'] = spider.name.replace('spider', '')
        return item

class DynamoPipeline(object):

    def open_spider(self, spider):
        self.dynamodb = DynamoDB()

    def process_item(self, item, spider):
        key =  '%s-%s' % (item['spider'], item['product_code'])
        self.dynamodb.set(key, dict(item))
        return item


