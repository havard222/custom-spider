# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/en/latest/topics/items.html

import scrapy


class CrawlerItem(scrapy.Item):
    # define the fields for your item here like:
    # name = scrapy.Field()
    spider = scrapy.Field()
    retailer = scrapy.Field()
    brand = scrapy.Field()
    name = scrapy.Field()
    product_code = scrapy.Field()
    text = scrapy.Field()
    price = scrapy.Field()
    price_retail = scrapy.Field()
    categories = scrapy.Field()
    original_url = scrapy.Field()
    images = scrapy.Field()
    version = '1.0'
