import re
from scrapy import Request
from scrapy.contrib.linkextractors.sgml import SgmlLinkExtractor
from scrapy.contrib.spiders import Rule
from scrapy.shell import inspect_response
from crawler.items import CrawlerItem
from crawler.spiders.basespider import BaseCrawlSpider

__author__ = 'mac'


class MaxboSpider(BaseCrawlSpider):

    name = 'maxbospider'
    #allowed_domains = 'www.maxbo.no'

    base_url = 'http://www.maxbo.no'
    start_urls = ['http://www.maxbo.no']
    rules = [
        Rule(SgmlLinkExtractor(allow=['/p/\d+$']), 'parse_product_from_response'),
        Rule(SgmlLinkExtractor(allow=['http://www.maxbo.no/Produktkatalog/']), 'parse_product_list'),
    ]
    download_delay = 2

    def parse_product_list(self, response):
        for l in response.css('a[href*="/p/"]::attr(href)').extract():
            yield Request(self.base_url + l, self.parse_product_from_response)
            return

    def parse_product_from_response(self, response):
        category_pattern = re.compile('Produktkatalog/(?:(.*?)/)p')
        product_code_pattern = re.compile('\d+$')

        try:
            tmp = re.search(category_pattern, response.url).groups()
            categories = [x.replace('-', ' ') for x in tmp[0].split('/')] if tmp else []
            product_code = re.search(product_code_pattern, response.url).group()
            name = response.css('.product-info-col h1::text').extract()[0]
            text = '\n'.join([x.strip() for x in response.css('.product-info-col p::text').extract()])
            tmp = response.css('#price::text').extract()
            price = tmp[0].strip().replace(' ', '').replace(u'\xa0', '').replace(',', '.') if tmp else False
            tmp = response.css('#comparison-price span::text').extract()
            price_retail = tmp[0].strip().replace(' ', '').replace(u'\xa0', '').replace(',', '.') if tmp else price
            images = [self.base_url + x for x in response.css('[data-primaryimagesrc]::attr(data-primaryimagesrc)').extract()]
            if price < price_retail:
                self.crawler.stats.inc_value('item_sale_count')
        except Exception, e:
            self.crawler.stats.inc_value('item_error_count')
            print '# ERROR: %s' % response.url
            raise

        return CrawlerItem(name=name,
                           product_code=product_code,
                           text=text,
                           price=price,
                           price_retail=price_retail,
                           original_url=response.url,
                           images=images)

