__author__ = 'mac'

import logging
logger = logging.getLogger('error')


class ExceptionLoggingMiddleware(object):
    def process_exception(self, request, exception):
        logger.exception('Exception handling request for ' + request.path)
