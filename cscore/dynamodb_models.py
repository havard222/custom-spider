from pynamodb.models import Model
from pynamodb.attributes import UnicodeAttribute, JSONAttribute, UTCDateTimeAttribute
from pynamodb.connection import TableConnection
from pynamodb.exceptions import DeleteError
from django.utils import timezone
from custom_spider import settings


class DDBCrawlerItemModel(Model):
    class Meta:
        table_name = 'customspider-{0}_generic_cache'.format(settings.SERVER_DESIGNATION)
        region = settings.AWS_DEFAULT_REGION
        write_capacity_units = 1
        read_capacity_units = 1

    key = UnicodeAttribute(hash_key=True)
    data = JSONAttribute(null=True)
    date_cached = UTCDateTimeAttribute(null=True)



class DynamoDB(object):

    def __init__(self):
        DDBCrawlerItemModel.create_table()
        
    def invalidate(self, key):
        t = TableConnection(DDBCrawlerItemModel.Meta.table_name, region=settings.AWS_DEFAULT_REGION)
        try:
            t.delete_item(key)
        except DeleteError:
            pass

    def set(self, key, data):
        self.invalidate(key)

        c = DDBCrawlerItemModel(key=key, data=data, date_cached=timezone.now())
        c.save()

        return c