from django.db import models

__author__ = 'mac'


class Retailer(models.Model):
    name = models.CharField(max_length=50, unique=True)


class Brand(models.Model):
    name = models.CharField(max_length=50, unique=True)


class GoogleTaxonomyCategory(models.Model):
    parent = models.ForeignKey('GoogleTaxonomyCategory', blank=True, null=True)
    name = models.CharField(max_length=50, unique=True)


class RetailerCategory(models.Model):
    retailer = models.ForeignKey('Retailer')
    name = models.CharField(max_length=50)
    retail_category = models.ForeignKey('GoogleTaxonomyCategory', blank=True, null=True)

    class Meta:
        unique_together = ('retailer', 'name')


class ProductImage(models.Model):
    # TODO: store images in s3
    original = models.ImageField()
    # TODO: Store scaled down version of original


class Product(models.Model):

    spider = models.CharField(max_length=50)
    retailer = models.ForeignKey('Retailer')
    brand = models.ForeignKey('Brand')
    name = models.CharField(max_length=100)
    product_code = models.CharField(max_length=50)
    description = models.CharField(max_length=300)
    price = models.DecimalField()
    retailer_price = models.DecimalField()
    retailer_categories = models.ManyToManyField('RetailerCategory')
    original_url = models.CharField(max_length=300)
    images = models.ManyToManyField('ProductImage')

    # TODO: Cache id + image_url of images and id + category_name of categories if product lookup takes time, consider using cloudfront +++ configuration
    #cache = models.JSONField()
